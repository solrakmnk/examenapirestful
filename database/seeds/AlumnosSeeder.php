<?php

use Illuminate\Database\Seeder;

class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_alumnos')->insert([
            'nombre' => "John",
            'ap_paterno' => "Dow",
            'ap_materno' => "Down",
            'activo' => 1,
        ]);
    }
}
