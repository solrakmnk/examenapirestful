<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    public $timestamps = false;
    protected $table="t_calificaciones";
    protected $primaryKey="id_t_calificaciones";
    protected $fillable=['id_t_alumnos','id_t_materias','calificacion','fecha_registro'];
    protected $appends=['nombre','apellido','materiaNombre'];
    protected $visible=['id_t_alumnos','nombre','apellido','materiaNombre','calificacion','fecha_registro'];


    public function alumno()
    {
        return $this->belongsTo(Alumno::class,"id_t_alumnos","id_t_alumnos");
    }
    public function materia()
    {
        return $this->belongsTo(Materia::class,"id_t_materias","id_t_materias");
    }
    public function getFechaRegistroAttribute(){
        return Carbon::parse($this->attributes['fecha_registro'])->format('d/m/Y');
    }
    public function getNombreAttribute(){
        return $this->alumno->nombre;
    }
    public function getApellidoAttribute(){
        return $this->alumno->ap_paterno;
    }
    public function getMateriaNombreAttribute(){
        return $this->materia->nombre;
    }

}
