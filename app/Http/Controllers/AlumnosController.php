<?php

namespace App\Http\Controllers;

use App\Alumno;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;




class AlumnosController extends ApiController
{

    public function show(Alumno $alumno)
    {
        $data=$alumno->calificaciones;
        $avg=$data->avg('calificacion');
        $data[]=['promedio'=>number_format($avg,2,".","")];
        return $this->showResponse($data);
    }


}
