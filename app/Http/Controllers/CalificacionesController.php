<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Calificacion;
use App\Materia;
use Illuminate\Http\Request;

class CalificacionesController extends ApiController
{

    public function store(Request $request)
    {
        $request->validate( [
            'id_t_alumnos' => 'required|int|exists:t_alumnos,id_t_alumnos',
            'id_t_materias' => 'required|int|exists:t_materias,id_t_materias',
            'calificacion' => 'required|numeric|between:0,10',
            'fecha_registro' => 'required|date',
        ]);

        $calificacion=new Calificacion($request->all());
        $calificacion->save();
        return $this->showMessage("Calificacion Registrada");
    }

    public function update(Request $request, Calificacion $calificacione)
    {
        $request->validate( [
            'id_t_alumnos' => 'int|exists:t_alumnos,id_t_alumnos',
            'id_t_materias' => 'int|exists:t_materias,id_t_materias',
            'calificacion' => 'numeric|between:0,10',
            'fecha_registro' => 'date',
        ]);

        $calificacione->fill($request->all());
        $calificacione->save();
        return $this->showMessage("Calificacion Actualizada");
    }

    public function destroy(Calificacion $calificacione)
    {
        //Si el id no existe se envia un mensaje de error en formato json manejado desde las excepciones.

        $calificacione->delete();
        return $this->showMessage("Calificacion Eliminada");
    }
}
