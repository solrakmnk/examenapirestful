<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $table="t_materias";
    protected $primaryKey="id_t_materias";


    public function calificaciones()
    {
        return $this->hasMany(Calificacion::class,"id_t_materias","id_t_materias");
    }
}
