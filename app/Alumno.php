<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table="t_alumnos";
    protected $primaryKey="id_t_alumnos";
    protected $visible=["id_t_alumnos","nombre","ap_paterno","ap_paterno","calificaciones"];

    public function calificaciones()
    {
        return $this->hasMany(Calificacion::class,"id_t_alumnos","id_t_alumnos");
    }

}
