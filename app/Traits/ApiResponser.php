<?php

namespace App\Traits;


use Illuminate\Database\Eloquent\Model;

trait ApiResponser
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showResponse($data, $code = 200)
    {
        return $this->successResponse($data, $code);
    }

    protected function showMessage($message)
    {
        return $this->successResponse(['success'=>'ok','msg' => $message], 200);
    }



}